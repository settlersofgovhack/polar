/* Functions for AJAX Calls */

var update_GDP_bar = function(year,month) {
	$.ajax({
		url: 'http://gh2015.scriptforge.org:5000/api/gdp/'+year+'/'+month,
		method: 'get',
		dataType: 'JSON',
//		jsonpCallback: 'callback',
		success: function(data) {
			var sumTotal = 0;
			var stateGDP = {WA:0,NT:0,SA:0,QLD:0,NSW:0,ACT:0,VIC:0,TAS:0}
			gdp = data;
			//console.log (gdp);
			for (var state in gdp){
				sumTotal += gdp[state];
				
			};
			for (var state in gdp){
				stateGDP[state]=((gdp[state]/sumTotal)*100).toFixed(2);
			}
			$("#pb-WA").css("width",stateGDP.WA+'%');
			$("#pb-NT").css("width",stateGDP.NT+'%');
			$("#pb-SA").css("width",stateGDP.SA+'%');
			$("#pb-QLD").css("width",stateGDP.QLD+'%');
			$("#pb-NSW").css("width",stateGDP.NSW+'%');
			$("#pb-ACT").css("width",stateGDP.ACT+'%');
			$("#pb-VIC").css("width",stateGDP.VIC+'%');
			$("#pb-TAS").css("width",stateGDP.TAS+'%');
			console.log (gdp);
			console.log (stateGDP.WA);
			console.log (sumTotal);
		}
	});
};
$(document).ready(function(){
	$("#Load_GDP_Data").click(function(){
		console.log ("This button works!");
		update_GDP_bar('2014','6');
	});
});
