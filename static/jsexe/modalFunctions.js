// GDP App - Modal Functions source code

$('#modalStateInformation').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var title = button.data('title') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text(title)
})

// JS with jQuery
var active = function(){
  $('.transition-slide-up').removeClass('inactive').addClass('active');
};

var inactive = function(){
  $('.transition-slide-up').removeClass('active').addClass('inactive');
};

// Create the main page slider

$('#timeLine').slider({
	animate: "fast",
	max: 104,
	min: 1,
	orientation: "horizontal",
	range: "true",
	step: 1,
	value: 1,
	values: 2
});

$('#timeLine').slider("enable");

var widget = $('#timeLine').slider("widget");
